# st19_i3_TextureModel

In recent years, many cities have been building digital cities to promote the process of urban informatization.

Besides the basic 3D city modeling, this project is more detail-oriented focusing on the shapes and patterns of buildings in the multiply campuses of University of Salzburg, aiming to build a 3D map to help users to find out where they want to go easily by observing the exterior texture.
